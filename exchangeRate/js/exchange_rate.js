/**
 * @author Alex Davila Alonso 2132425
 */


document.addEventListener('DOMContentLoaded',function(){

        initialize_Select_Options_data();
        init();

        
});

/**
 * This method initializes global objects and evemt listeners
 */
function init(){
     let btn = document.getElementById("convert");
     enable_disable_button(btn,false);
     let fromOptions = document.getElementById("from");
     fromOptions.addEventListener('blur',function(){validateForm();});

     let toOptions = document.getElementById("to");
     toOptions.addEventListener('blur',function(){validateForm();});

     let amount = document.getElementById("amount");
     amount.addEventListener('blur',function(){validateForm();});

    
    

    let table = document.getElementById("table");
    table.style.display = "none";
    let tbody = document.getElementById("table").getElementsByTagName("tbody")[0];
    btn.addEventListener('mouseenter',()=>{
        btn.style.color = "white";
        btn.style.background = "rgb(255,255,255)";
        btn.style.background = "radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(0,0,0,1) 0%, rgba(108,108,108,1) 100%)";
    
    })
    btn.addEventListener('mouseleave',()=>{
        btn.style.color = "black";
        btn.style.background = "rgb(255,255,255)";
        btn.style.background = "radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(108,108,108,1) 99%)";
    })
    btn.addEventListener('click',()=>{
        let row = tbody.insertRow(tbody.length);
    getData(row);
})
}

/**
 * This method calls the createUrlObject to assemble the full URL to be used
 * 
 * @param {String} from the currency you wish to convert
 * @param {String} to the currency you wish to convert to
 * @param {Number} amount the amount you wish to convert 
 * @returns the correct URL to do the conversions
 */
function getAbsoluteRequestURL(from,to,amount){
    let urlObj = createUrlObject(from,to,amount);
    return urlObj.absoluteURL;

}

/**
 * uses the fetch API to request data from the exchangerate url
 * when we have the data needed we call the displayResults to generate
 * the table
 */
function getData(parent){
    let fromOptions = document.getElementById("from");
    let fromValue = fromOptions.value;
    let toOptions = document.getElementById("to");
    let toValue = toOptions.value;
    let amount = document.getElementById("amount");
    let amountValue = amount.value;
    let url = getAbsoluteRequestURL(fromValue,toValue,amountValue)

    
    fetch(url).then((resp)=>{
        if(resp.status === 200){
            return resp.json();
        }
    }).then((result)=>{
        let apiString = JSON.stringify(result);
        let apiObj = JSON.parse(apiString);

        let from = apiObj.query.from;
        let to = apiObj.query.to;
        let rate = apiObj.info.rate;
        let roundRate = Math.round(rate * 100) / 100;
        let amount = apiObj.query.amount;
        let payment = apiObj.result;
        let roundPayment = Math.round(payment * 100) / 100;
        let fullDate = new Date(); 
        let date = apiObj.date;
        let time = fullDate.getHours()+":"+fullDate.getMinutes()+":"+ fullDate.getSeconds();

        let currentDate = date+"-"+time;

      
        displayResults(parent,from,to,roundRate,amount,roundPayment,currentDate);
    }).catch((err)=>{
        console.log("error!",err);
    })
}

/**
 * This function displays the results fetched in the getData and displays them is table
 * always creating a new row with new converted currency
 * 
 * @param {HTMLElement} parent 
 * @param {Object} from 
 * @param {Object} to 
 * @param {Object} rate 
 * @param {Object} amount 
 * @param {Object} payment 
 * @param {Object} date 
 */
function displayResults(parent,from,to,rate,amount,payment,date){
    let table = document.getElementById("table");
    table.style.display = "block";
    let cell1 = parent.insertCell(0);
    cell1.innerHTML = from;
    let cell2= parent.insertCell(1);
    cell2.innerHTML = to;
    let cell3 = parent.insertCell(2);
    cell3.innerHTML = rate;
    let cell4 = parent.insertCell(3);
    cell4.innerHTML = amount;
    let cell5 = parent.insertCell(4);
    cell5.innerHTML = payment;
    let cell6 = parent.insertCell(5);
    cell6.innerHTML = date;  

}

/**
 * This method creates an object with the parts of a URL that is going to be assembled
 * 
 * @param {String} from the currency you wish to convert
 * @param {String} to the currency you wish to convert to
 * @param {Number} amount the amount you wish to convert
 * @returns a URL object which accesses the API to do the conversions
 */
function createUrlObject(from,to,amount){
    let mainURI = `https://api.exchangerate.host/convert`;
    let fromQuery = `from=`+from;
    let toQuery = `&to=`+to;
    let amountQuery = `&amount=`+amount;
    
    
    let urlObject = {
        mainURL: mainURI,
        fromQry: fromQuery,
        toQry: toQuery,
        amountQry: amountQuery,
        absoluteURL: mainURI+"?"+fromQuery+toQuery+amountQuery
    }

    return urlObject;
}
/**
 * this function checks all values of the select button and checks if they are filled
 * if not the button stays disabled
 * if they are valid the button is enabled
 * 
 */
function validateForm(){ 
    let fromOptions = document.getElementById("from");
    let fromValue = fromOptions.value;
    let toOptions = document.getElementById("to");
    let toValue = toOptions.value;
    let amount = document.getElementById("amount");
    let amountValue = amount.value;
        let fromValid = false;
        let toValid = false;
        let amountValid = false;


        if (!(fromValue === "----")) {
            fromValid = true;
        }
        
        if (!(toValue === "----")) {
      
            toValid = true;
        }
        
        if (amountValue > 0) {  

            amountValid = true;
        } 
        
        let btn = document.getElementById("convert");

        
          if(fromValid && toValid && amountValid){
              enable_disable_button(btn,true);
          }
          else{
              enable_disable_button(btn,false);
          }
        
  }


