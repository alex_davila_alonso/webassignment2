// Alex Davila Alonso 2132425
/**
 * This method initializes the options of the select elements in the form
 */
function initialize_Select_Options_data(){
    jsonFile = "../json/currency.json";


fetch(jsonFile).then((resp)=>{
    if(resp.status === 200){
        return resp.json()
    }
}).then((res)=>{
    let stringObj = JSON.stringify(res);
    let obj = JSON.parse(stringObj);
    for(let object of Object.values(obj)){
        let from = document.getElementById("from");
        let to = document.getElementById("to");

        let toOption = document.createElement("option");
        toOption.setAttribute('value', object.code);
        let toOptionText = document.createTextNode(object.name);
        toOption.appendChild(toOptionText);

        let fromOption = document.createElement("option");
        fromOption.setAttribute('value', object.code);
        let fromOptionText = document.createTextNode(object.name);
        fromOption.appendChild(fromOptionText);

        
        to.appendChild(toOption)
        from.appendChild(fromOption);
    }
}).catch((err)=>{
    console.log("err",err)
})
}

/**
 * this function takes a boolean as parameter to see if the button should be enabled
 * it it should it makes the button style different and in the exchange rate that button
 * will be clickable and hoverable
 * 
 * @param {HTMLElement} btn the button that we want to apply the function to
 * @param {Boolean} valid the boolean telling us if the form is valid
 */
function enable_disable_button (btn,valid){
    if(valid){
        btn.removeAttribute('disabled');
        btn.style.color = "black";
    } else {
        btn.setAttribute('disabled','disabled');
        btn.style.background = "rgb(255,255,255)";
        btn.style.background = "radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(108,108,108,1) 99%)";
        btn.style.color = "lightgrey";
    }
}







